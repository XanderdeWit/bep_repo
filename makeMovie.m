%NOTE: dtime is delay in sec for GIF. dtPerSlab is convective time units between consecutive slabs
function makeMovie(simname,getData,thetas,rs,dtPerSlab,n1,n2,clim,dtime,plotStyle,figSize,d,dataDir,rwall)
h = figure;
R=0.1*round(10*max(rs));
set(gcf, 'Position',  [100, 100, figSize(1), figSize(2)])
axis tight manual % this ensures that getframe() returns a consistent size
filename = 'testAnimated.gif';

plt=drawHorCross(thetas,rs,getData(simname,n1,d,dataDir)',clim,plotStyle);
if rwall
    hold on
    plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
    plot(R*cos(0:pi/100:2*pi),R*sin(0:pi/100:2*pi),'k-','LineWidth',0.5)
    hold off
end

xlim([-R,R])
ylim([-R,R])

set(gca,'xtick',[])
set(gca,'ytick',[])

minData=Inf;
maxData=-Inf;
totalMinData=0;
totalMaxData=0;

for n = n1:n2
    data=getData(simname,n,d,dataDir);
    
    %To determine clim
    currentMin=min(min(data));
    currentMax=max(max(data));
    totalMinData=totalMinData + currentMin;
    totalMaxData=totalMaxData + currentMax;
    if currentMin < minData
        minData = currentMin;
    end
    if currentMax > maxData
        maxData = currentMax;
    end
    
    if strcmp(plotStyle,'pcolor')
        plt.CData=dataMakePeriodic(data');
    else
        plt.ZData=dataMakePeriodic(data');
    end
    
    %Put timer in title
    title(['t=' sprintf('%.1f', dtPerSlab*(n-n1))])
    
    drawnow
    % Capture the plot as an image
    frame = getframe(h);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    % Write to the GIF File
    if n == n1
        imwrite(imind,cm,filename,'gif','Loopcount',inf,'DelayTime',dtime);
    else
        imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',dtime);
    end
end

%To determine clim
disp(['absolute minData=' num2str(minData)])
disp(['absolute maxData=' num2str(maxData)])
disp(['average minData=' num2str(totalMinData / (n2-n1+1))])
disp(['average maxData=' num2str(totalMaxData / (n2-n1+1))])

end