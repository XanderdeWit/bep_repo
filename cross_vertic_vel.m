%Cross sections of vertical velocity

%Choose which simulation to view
nsim=1;

simname=char(gSimnames(nsim));

vz=h5read([gDataDir simname '/continua_q3.h5'],'/q3');

%last rows are all zeros in theta and r direction
vz=vz(1:(size(vz,1)-1),1:(size(vz,2)-1),1:(size(vz,3)));

zs=d.zs(1:end-1);

plotHorCross(d.thetams,d.rms,zs,vz,'vertical velocity','auto','pcolor')
%plotHorCross(d.thetams,d.rms,zs,vz,'vertical velocity',[-0.4 0.4],'contourf')
plotVerticCross(d.thetams,d.rms,zs,vz,'vertical velocity','auto','pcolor')

%plotIsoSurf(d.thetams,d.rms(1:230),zs,vz(:,1:230,:),0.02,'vertical velocity')