%Create spectrum of virtual thermistor to match experiment

%Choose which simulation to view
nsim=5;

ome_scs=[0.0144    0.0234    0.0309    0.0584    0.0637];

ome_sc=ome_scs(nsim);

simname=char(gSimnames(nsim));

n1=gN1s(nsim);
n2=gN2s(nsim);

slabname='slab8'; %mid-height

therm1s=zeros(n2-n1+1,1);
therm2s=zeros(n2-n1+1,1);

for n=n1:n2
    temp=h5read([gDataDir simname '/stst3/' slabname 'dens_' sprintf('%08d', n) '.h5'],'/var');
    therm1s(n-n1+1)=temp(1,end);
    therm2s(n-n1+1)=temp(round(end/2),end);
end


%fourier transform
T=0.5; %sampling period
Fs=1/T; %sampling frequency
L=length(therm1s); %length of signal
t=(0:L-1)*T; %time vector
f = 2*pi*Fs*(0:(L/2))/L; %frequency vector

fft1=fft(therm1s);
fft2=fft(therm2s);

P2_1 = abs(fft1/L);
P1_1 = P2_1(1:L/2+1);
P1_1(2:end-1) = 2*P1_1(2:end-1);

P2_2 = abs(fft2/L);
P1_2 = P2_2(1:L/2+1);
P1_2(2:end-1) = 2*P1_2(2:end-1);

figure
loglog(f,P1_1)
hold on
plot([ome_sc ome_sc], ylim,'r-');
hold off
xlabel('$\omega \tau_c$')
ylabel('Amplitude (arb. units)')
figure
loglog(f,P1_2)
hold on
plot([ome_sc ome_sc], ylim,'r-');
hold off
xlabel('$\omega \tau_c$')
ylabel('Amplitude (arb. units)')

figure
plot(t,therm1s)
hold on
plot(t,therm2s)
xlabel('$t/\tau_c$')
ylabel('$T/\Delta T$')