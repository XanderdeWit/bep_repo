%Cross sections and iso surfaces of horizontal kinetic energy

%Choose which simulation to view
nsim=1;

simname=char(gSimnames(nsim));

vth=h5read([gDataDir simname '/continua_q1.h5'],'/q1');
qr=h5read([gDataDir simname '/continua_q2.h5'],'/q2');

%last rows are all zeros in theta and r direction
qr=qr(1:(size(qr,1)-1),1:(size(qr,2)-1),1:(size(qr,3)));
vth=vth(1:(size(vth,1)-1),1:(size(vth,2)-1),1:(size(vth,3)));

%calculate kinetic energy
horKin=zeros(size(qr));
for i=1:length(d.zms)
    horKin(:,:,i)=getHorKin(vth(:,:,i),qr(:,:,i),d);
end

plotHorCross(d.thetams,d.rms,d.zms,horKin,'horizontal kinetic energy','auto','pcolor')
plotVerticCross(d.thetams,d.rms,d.zms,horKin,'horizontal kinetic energy','auto','pcolor')
plotIsoSurf(d.thetams,d.rms,d.zms,horKin,0.0001,'horizontal kinetic energy')
