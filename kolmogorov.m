%Display kolmogorov and batchlor scale analysis

%Choose which simulation to view
nsim=5;

simname=char(gSimnames(nsim));

pr=gPr;
ra=gRas(nsim);


kinDiss=h5read([gDataDir simname '/stafield_data.h5'],'/kindiss_mean');
averagingTime=h5read([gDataDir simname '/stafield_master.h5'],'/averaging_time');
kinDiss=kinDiss./double(averagingTime);

%Plot kinetic dissipation
figure
drawVerticCross(d.rms,d.zms,kinDiss','auto','contourf');
title('Kinetic dissipation')

%Calculate r-averaged kinetic dissipation over full and 3/4 radius
kinDiss_rmean=mean(kinDiss);
kinDiss_rmean3q=mean(kinDiss(1:floor(size(kinDiss,1)*0.75),:));

figure
scatter(kinDiss_rmean,d.zms,10,'s','filled')
title('Kinetic dissipation (r,theta average)')
ylabel('z')
xlabel('Kinetic dissipation')
grid on
box on
hold on
scatter(kinDiss_rmean3q,d.zms,10,'s','filled')
hold off
legend({'Full r','3/4 r'},'Location','southeast')

%Plots of number of kolmogs per cell
kolmogScales=calculateKolmogorov(kinDiss',pr,ra);
lengthsZ=repmat(diff(d.zs),[1,d.nrm]);
lengthsR=repmat(diff(d.rs)',[d.nzm,1]);
lengthsTheta=repmat(2*pi*d.rms'/d.ntheta,[size(d.zms),1]);
lengthsDiag=sqrt(lengthsZ.^2+lengthsR.^2+lengthsTheta.^2);
lengthsMax=max(max(lengthsZ,lengthsR),lengthsTheta);

figure
drawVerticCross(d.rms,d.zms,lengthsZ./kolmogScales,'auto','contourf');
title('Number of kolmogorov scales per cell in z direction')
figure
drawVerticCross(d.rms,d.zms,lengthsR./kolmogScales,'auto','contourf');
title('Number of kolmogorov scales per cell in r direction')
figure
drawVerticCross(d.rms,d.zms,lengthsTheta./kolmogScales,'auto','contourf');
title('Number of kolmogorov scales per cell in theta direction')
figure
drawVerticCross(d.rms,d.zms,lengthsDiag./kolmogScales,'auto','contourf');
title('Number of kolmogorov scales per cell in diagonal direction')
figure
drawVerticCross(d.rms,d.zms,lengthsMax./kolmogScales,'auto','contourf');
title('Number of kolmogorov scales per cell in maximal direction')
figure
drawVerticCross(d.rms,d.zms,lengthsMax./kolmogScales.*sqrt(pr),[0 5],'contourf');
title('Number of Batchelor scales per cell in maximal direction')
xlabel('r')
ylabel('z')
xlim([0,d.R])
ylim([0,d.H])

function eta=calculateKolmogorov(kinDiss,prandtl,rayleigh)
    eta=(prandtl/rayleigh).^(3/8).*kinDiss.^(-1/4);
end