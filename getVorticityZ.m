function vorticityZ = getVorticityZ(vth,qr,d)

qrm=d.qrmFromQr(qr);
vrm=qrm./d.rmsMesh;

%drvthDr, defined at position qtheta
rvth=d.rmsMesh.*vth;
drvthDr=gradient(rvth,1,2)./repmat(gradient(d.rms'),length(d.thetams),1);

%dvrmDtheta, defined at position qtheta
diffPthetaMs=diffP(d.thetams');
diffPthetaMs(1)=diffPthetaMs(1)+2*pi;
dvrmDtheta=diffP(vrm)./repmat(diffPthetaMs,1,length(d.rms));

%vorticityz, defined at position qtheta
vorticityZ=(drvthDr-dvrmDtheta)./d.rmsMesh;

%around axis it goes wrong, so
vorticityZ(:,1)=vorticityZ(:,2);

function out=diffP(in)
    diffFirst=in(1,:)-in(end,:);
    out=[diffFirst;diff(in)];
end

end

