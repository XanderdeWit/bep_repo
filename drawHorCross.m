function h=drawHorCross(thetas,rs,data,dataLim,plotStyle)

pdata=dataMakePeriodic(data);
pthetas=[thetas,(thetas(end)+thetas(end)-thetas(end-1))];

[thetamesh,rmesh]=meshgrid(pthetas,rs);
[xmesh,ymesh]=pol2cart(thetamesh,rmesh);

if strcmp(plotStyle,'pcolor')
    h=pcolor(xmesh,ymesh,pdata);
    shading interp
    colormap(getRudieColorMap())
    colorbar('TickLabelInterpreter','latex')
    caxis(dataLim)
elseif strcmp(plotStyle,'contourf')
    [~,h]=contourf(xmesh,ymesh,pdata);
    colormap(getRudieColorMap())
    colorbar('TickLabelInterpreter','latex')
    caxis(dataLim)
elseif strcmp(plotStyle,'surf')
    h=surf(xmesh,ymesh,pdata);
    zlim(dataLim)
else
    disp('Wrong plot style!')
    h=-1;
end

end

