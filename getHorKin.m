function horKin = getHorKin(vth,qr,d)

qrm=d.qrmFromQr(qr);
vrm=qrm./d.rmsMesh;
vthm=d.vthmFromVth(vth);

horKin=vrm.^2+vthm.^2;

end

