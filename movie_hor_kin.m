%Movie of horizontal cross section of horizontal kinetic energy

%Choose which simulation to view
nsim=1;

simname=char(gSimnames(nsim));

n1=gN1s(nsim);
n2=gN2s(nsim);

%The color axis needs to be calibrated, minimum and maximum are printed after rendering
%clim='auto';
clim=[0 0.0004];

nrwall=gNrwalls(nsim);
rwall=d.rms(nrwall);

makeMovie(simname,@getData,d.thetams,d.rms,0.5,n1,n2,clim,0.1,'pcolor',[800,650],d,gDataDir,rwall)

function data=getData(simname,n,d,dataDir)
slabname='slab8'; %mid-height

vth=h5read([dataDir simname '/stst3/' slabname 'q1_' sprintf('%08d', n) '.h5'],'/var');
qr=h5read([dataDir simname '/stst3/' slabname 'q2_' sprintf('%08d', n) '.h5'],'/var');

data=getHorKin(vth,qr,d);

end