function decorTime = autoCor(ts,showGraph)
acor=xcorr(ts.Data-mean(ts.Data));
acor=acor(length(ts.Time):end)/acor(length(ts.Time));

if showGraph
    figure
    scatter(ts.Time,acor,10,'s','filled')
    title('Auto-correlation Nusselt number')
    grid on
    box on
end

%find first zero crossing
zci = @(v) find(v(:).*circshift(v(:), [-1 0]) <= 0);
zcis=zci(acor);
decorTime=ts.Time(zcis(1));

end

