%Performance benchmark of code

%TU/e Xander de Wit BEP cylindrical data
%           Nodes   Cores   Real    CPU
perfData=[  1       24      1.66    4.15;
            2       48      2.62    3.27;
            3       72      3.12    2.60;
            4       96      2.58    1.61;
            5       120     4.20    2.10;
            6       144     4.55    1.90;
            7       168     6.21    2.22;
            8       192     7.22    2.26;
            9       216     7.49    2.08;
            10      240     7.59    1.90;
            11      264     8.65    1.97;
            12      288     8.52    1.78;
            13      312     8.67    1.67;
            14      336     9.72    1.73];

figure
hold on

yyaxis left
scatter(perfData(:,2),perfData(:,3))%,60,'*')
ylabel('Real time speed (iterations/minute)')
%set(gca,'yscale','log')
ylim([0 12])

yyaxis right
scatter(perfData(:,2),perfData(:,4))
ylabel('CPU-time speed (iterations/CPU-hour)')
%set(gca,'yscale','log')
ylim([0 4.5])

hold off
title('Performance of the numerical code')
xlabel('Number of cores')
grid on
box on
%set(gca,'xscale','log')