%Analyze the local Nusselt numbers, based on stafield data of vz*T

%add a slider bar to select height
addSlider=true;

%Analyzing case nsim1 till nsim2
nsim1=1;
nsim2=length(gSimnames);

localNus=zeros(nsim2-nsim1+1,3,d.nrm,d.nzm);

Nui=1; %plot the local Nu (Nui=1) together in single figure, change for NuInner (=2) or NuWall (=3)
NuiName='Local Nu';

for i=1:(nsim2-nsim1+1)
    nsim=nsim1+i-1;

    simname=char(gSimnames(nsim));
    ra=gRas(nsim);
    nrwall=gNrwalls(nsim);
    rwall=d.rms(nrwall);

    %get average vz*T from stafield
    vzTTotal=h5read([gDataDir simname '/stafield_data.h5'],'/densq3_mean');
    averagingTime=h5read([gDataDir simname '/stafield_master.h5'],'/averaging_time');
    vzT=vzTTotal./double(averagingTime);

    %calculate Nu
    lNu=sqrt(ra*gPr)*vzT;
    
    %calculate local, inner and wall Nu number
    weightsNuLocal=sum(d.horAreas,2);%local Nusselt number is weighted by the areas of the ring at r
    localNus(i,1,:,:)=lNu; %local Nu
    for nz=1:d.nzm
        localNus(i,2,:,nz)=getRunningAverage(lNu(:,nz),weightsNuLocal); %inner Nu
        localNus(i,3,:,nz)=flip(getRunningAverage(flip(lNu(:,nz)),flip(weightsNuLocal))); %wall Nu
    end

    %Vertical crossection of local Nu
    figure
    drawVerticCross(d.rms,d.zms,squeeze(localNus(i,1,:,:))','auto','pcolor');
    title(['Local Nusselt number for Ra=' num2str(gRas(nsim),'%1.1e')])
    %add rwall
    line([rwall,rwall],ylim,'Color','m')
    
    %Plot total, local, inner and wall Nu at certain z
    nz=round(d.nzm/2); %mid-height
    figure
    hold on
    hTotal=scatter(d.rms,repmat(localNus(i,2,end,nz),d.nrm,1),10,'s','filled');
    hLocal=scatter(d.rms,localNus(i,1,:,nz),10,'s','filled');
    hInner=scatter(d.rms,localNus(i,2,:,nz),10,'s','filled');
    hWall=scatter(d.rms,localNus(i,3,:,nz),10,'s','filled');
    xlabel('r')
    ylabel('Nu')
    legend({'Nu total','Nu local','Nu inner', 'Nu wall'},'Location','northwest')
    updateTitle(gRas(nsim),d.zms(nz));
    grid on
    box on 
    %add rwall
    hLine=line([rwall,rwall],ylim);

    %add slider
    if addSlider
        slider = uicontrol('style','slider','min',1,'max',d.nzm,'value',nz,'position',[10 10 300 10]);
        addlistener(slider,'ContinuousValueChange',@(hObject, event) updatePlot(round(get(slider,'Value')),hTotal,hLocal,hInner,hWall,hLine,localNus,nrwall,ra,d,i));
    end
end

%plot local Nus together in a single figure at certain z
nz=round(d.nzm/2); %mid-height
figure
hold on
for i=1:(nsim2-nsim1+1)
    hs(i)=scatter(d.rms,(d.rms/0.1) .* squeeze(localNus(i,Nui,:,nz)),10,'s','filled');
end
xlabel('r')
ylabel('Nu')
legend(gSimsLegend(nsim1:nsim2),'Location','northwest')
updateTitleTogether(NuiName,d.zms(nz));
grid on
box on

%add slider
if addSlider
    slider = uicontrol('style','slider','min',1,'max',d.nzm,'value',nz,'position',[10 10 300 10]);
    addlistener(slider,'ContinuousValueChange',@(hObject, event) updatePlotTogether(round(get(slider,'Value')),hs,Nui,NuiName,localNus,d,nsim1,nsim2));
end
    
function updatePlot(nz,hTotal,hLocal,hInner,hWall,hLine,localNus,nrwall,ra,d,i)
    hTotal.YData=repmat(localNus(i,2,end,nz),d.nrm,1);
    hLocal.YData=localNus(i,1,:,nz);
    hInner.YData=localNus(i,2,:,nz);
    hWall.YData=localNus(i,3,:,nz);
    
    hLine.YData=ylim;
    
    updateTitle(ra,d.zms(nz));
    
    disp(['Found nuTotal = ' num2str(localNus(i,2,end,nz)) ' and nuInner = ' num2str(localNus(i,2,nrwall,nz)) ' for Ra = ' num2str(ra,'%1.1e') ' and z = ' num2str(d.zms(nz),2)])
end

function updateTitle(ra,z)
    title(['Local Nusselt numbers for Ra = ' num2str(ra,'%1.1e') 'and z = ' num2str(z,2)])
end

function updatePlotTogether(nz,hs,Nui,NuiName,localNus,d,nsim1,nsim2)
    for i=1:(nsim2-nsim1+1)
        hs(i).YData=localNus(i,Nui,:,nz);
    end
    
    updateTitleTogether(NuiName,d.zms(nz));
end

function updateTitleTogether(NuiName,z)
    title([NuiName ' (averaged in theta direction) for z = ' num2str(z,2)])
end