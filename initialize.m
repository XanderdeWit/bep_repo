%NOTE: this script must be run before running any other scripts
%In this file, all global variables are set and the grid is initialized.

%Global Prandtl number
gPr=5.2;

%The directory of the data
gDataDir='/Volumes/HD/BEP_data/stat_sims/';

%The directories of the 5 simulations within the data directory
gSimnames={'sim5e10','sim7e10','sim13e10','sim31e10','sim43e10'};
%The respective Rayleigh numbers
gRas=[5.0e10, 7.0e10, 1.364e11, 3.18e11, 4.312e11];
gSimsLegend={'$\textrm{Ra}=5.0\times10^{10}$','$\textrm{Ra}=7.0\times10^{10}$','$\textrm{Ra}=1.4\times10^{11}$','$\textrm{Ra}=3.2\times10^{11}$','$\textrm{Ra}=4.3\times10^{11}$'};
%The respective Rossby numbers
gRos=[9.8e-3, 1.2e-2, 1.6e-2, 2.5e-2, 2.9e-2];
%The r-index of the transition from inner to wall region
gNrwalls=[282, 280, 273, 265, 259];
%The first and last slab number in the stst3 directory
gN1s=[513, 385, 1117, 1120, 1050];
gN2s=[1659, 1421, 1748, 1851, 1770];

%Set typeface
set(0,'defaultTextInterpreter','latex')
set(0,'defaultLegendInterpreter','latex')
set(0,'defaultAxesTickLabelInterpreter','latex')

set(0, 'DefaultAxesFontSize', 16)

set(0,'DefaultLegendAutoUpdate','off')

%Initialize the grid
make_domain;