%Visualize single slabs

%index of slabs used in report
%n=[800,457,1195,1396,1396]

%Choose which simulation to view
nsim=1;

simname=char(gSimnames(nsim));
%simname='sim5e10_SF';

slabname='slab8'; %mid-height
n=800;
ra=gRas(nsim);
pr=gPr;
rwall=d.rms(gNrwalls(nsim));

vz=h5read([gDataDir simname '/stst3/' slabname 'q3_' sprintf('%08d', n) '.h5'],'/var');
vth=h5read([gDataDir simname '/stst3/' slabname 'q1_' sprintf('%08d', n) '.h5'],'/var');
qr=h5read([gDataDir simname '/stst3/' slabname 'q2_' sprintf('%08d', n) '.h5'],'/var');
temp=h5read([gDataDir simname '/stst3/' slabname 'dens_' sprintf('%08d', n) '.h5'],'/var');

%Vz
figure
hold on
drawHorCross(d.thetams,d.rms,vz','auto','pcolor');
plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2) %indicate rwall
mcaxis=caxis;
mampl=(mcaxis(2)-mcaxis(1))/2;
caxis([-mampl,mampl]) %make color axis symmetric
title('Vertical velocity')
hold off
box on

%Temperature
figure
hold on
drawHorCross(d.thetams,d.rms,temp','auto','pcolor');
plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
mcaxis=caxis;
mampl=(mcaxis(2)-mcaxis(1))/2;
caxis(0.5+[-mampl,mampl])
title('Temperature')
hold off
box on

%Vz * Temperature (proportional to Nu)
figure
hold on
drawHorCross(d.thetams,d.rms,temp'.*vz','auto','pcolor');
plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
mcaxis=caxis;
mampl=(mcaxis(2)-mcaxis(1))/2;
caxis([-mampl,mampl])
title('Vz * T (proportional to Nu)')
hold off
box on

%Vertical vorticity
figure
hold on
drawHorCross(d.thetas(1:end-1),d.rms,getVorticityZ(vth,qr,d)','auto','pcolor');
plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
mcaxis=0.7*caxis; %don't show full color axis, because of high vorticity near the wall
mampl=(mcaxis(2)-mcaxis(1))/2;
caxis([-mampl,mampl])
title('Vertical vorticity')
hold off
box on

%Horizontal kinetic energy
figure
hold on
drawHorCross(d.thetams,d.rms,getHorKin(vth,qr,d)','auto','pcolor');
plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
title('Horizontal kinetic energy')
hold off
box on

%Vth
figure
hold on
drawHorCross(d.thetams,d.rms,vth','auto','pcolor');
plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
mcaxis=caxis;
mampl=(mcaxis(2)-mcaxis(1))/2;
caxis([-mampl,mampl])
title('Azimuthal velocity')
hold off
box on

%Vr
qrm=d.qrmFromQr(qr);
vrm=qrm./d.rmsMesh;

figure
hold on
drawHorCross(d.thetams,d.rms,vrm','auto','pcolor');
plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
mcaxis=caxis;
mampl=(mcaxis(2)-mcaxis(1))/2;
caxis([-mampl,mampl])
title('Radial velocity')
hold off
box on


%Fit cosine to vz
nrmode=findIn(d.rms,(max(d.rms)+rwall)/2)
rmode=d.rms(nrmode)

%make a quick plot
figure
hold on
drawHorCross(d.thetams,d.rms,vz','auto','pcolor');
plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
plot(rmode*cos(0:pi/100:2*pi),rmode*sin(0:pi/100:2*pi),'r-.','LineWidth',2)
mcaxis=caxis;
mampl=(mcaxis(2)-mcaxis(1))/2;
caxis([-mampl,mampl])
title('Vz')
hold off
box on

vzmode=vz(:,nrmode)';

[cosfit,xfit]=fitCos(vzmode,d.thetams);

figure
hold on
scatter(d.thetams,vzmode,10,'s','filled')
plot(d.thetams,cosfit(xfit,d.thetams),'r-.','LineWidth',2)
hold off
grid on
box on
title(['Vz at r=' num2str(rmode)])
xlim([0,2*pi])