%Make a plot of the different boundary layer thicknesses

%TU/e Xander de Wit BEP cylindrical data
%           Ra          vthRMS      vzRMS       tempRMS     vzRMSdip    Ek1/3       Ek1/4
stewBL=[    5.0e10      0.003120    0.0043557   0.0071098   0.0136      0.00584804  0.0211474;
            7.0e10      0.003496    0.0047396   0.0093829   0.0141      0.00584804  0.0211474;
            1.364e11    0.003684    0.0047396   0.013532    0.0157      0.00584804  0.0211474;
            3.18e11     0.003684    0.004165    0.011313    0.0176      0.00584804  0.0211474;
            4.312e11    0.003684    0.0039751   0.011532    0.0190      0.00584804  0.0211474];
        
figure
hold on
scatter(stewBL(:,1),stewBL(:,2),50,'d','filled')
scatter(stewBL(:,1),stewBL(:,3),50,'d','filled')
scatter(stewBL(:,1),stewBL(:,4),50,'d','filled')
scatter(stewBL(:,1),stewBL(:,5),50,'d','filled')
scatter(stewBL(:,1),stewBL(:,6),40,'o')
%scatter(stewBL(:,1),stewBL(:,7),40,'o')
hold off
legend({'$v_\theta$ RMS peak','$v_z$ RMS peak','T RMS peak','$v_z$ RMS dip','$(2 \textrm{Ek} )^{1/3}$'},'Location','eastoutside')
title('Side-wall boundary layer thickness')
xlabel('Ra')
ylabel('Thickness')
set(gca,'xscale','log')
xlim([4e10 5e11])
grid on
box on

%Fit power law to vz RMS dip
x=stewBL(:,1);
y=stewBL(:,5);
[~,P,cP]=linearFit(log(x),log(y));
power=P(1)
dPower=[P(1)-cP(1,1),cP(2,1)-P(1)] %uncertainty in power
base=exp(P(2))
dBase=[base-exp(cP(1,2)),exp(cP(2,2))-base] %uncertainty in prefactor

%plot on the graph
hold on
plot(x,base*x.^power,'r-.');
hold off