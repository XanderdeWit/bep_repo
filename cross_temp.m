%Cross sections and iso surfaces of temperature

%Choose which simulation to view
nsim=1;

simname=char(gSimnames(nsim));

temp=h5read([gDataDir simname '/continua_dens.h5'],'/dens');

%last rows are all ones in theta direction and duplicate in r direction
temp=temp(1:(size(temp,1)-1),1:(size(temp,2)-1),1:(size(temp,3)));

plotHorCross(d.thetams,d.rms,d.zms,temp,'temperature','auto','pcolor')
plotVerticCross(d.thetams,d.rms,d.zms,temp,'temperature','auto','pcolor')
plotIsoSurf(d.thetams,d.rms,d.zms,temp,[0.58, 0.42],'temperature')
