%Cross sections and iso surfaces of vertical vorticity

%Choose which simulation to view
nsim=1;

simname=char(gSimnames(nsim));

vth=h5read([gDataDir simname '/continua_q1.h5'],'/q1');
qr=h5read([gDataDir simname '/continua_q2.h5'],'/q2');

%last rows are all zeros in theta and r direction
qr=qr(1:(size(qr,1)-1),1:(size(qr,2)-1),1:(size(qr,3)));
vth=vth(1:(size(vth,1)-1),1:(size(vth,2)-1),1:(size(vth,3)));

%Calculate vertical vorticity
vorticity=zeros(size(qr));
for i=1:length(d.zms)
    vorticity(:,:,i)=getVorticityZ(vth(:,:,i),qr(:,:,i),d);
end

plotHorCross(d.thetas(1:end-1),d.rms,d.zms,vorticity,'vertical vorticity',[-20,20],'pcolor')
plotVerticCross(d.thetas(1:end-1),d.rms,d.zms,vorticity,'vertical vorticity',[-20,20],'pcolor')
plotIsoSurf(d.thetas(1:end-1),d.rms,d.zms,vorticity,20,'vertical vorticity')
