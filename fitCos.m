function [cosfit,xfit] = fitCos(vzmode,thetams)
    cosfit = @(x,xdata) x(1) .* cos(xdata - x(2)) + x(3);
    [vzmax,vzimax]=max(vzmode);
    [vzmin,~]=min(vzmode);
    guess = [0.5*(vzmax-vzmin)/2,thetams(vzimax),mean(vzmode)];
    xfit = lsqcurvefit(cosfit,guess,thetams,vzmode,[0,-Inf,-Inf],[],optimset('Display','off'));
end