function plotHorCross(thetas,rs,zs,values,valueTitle,fixedAxis,plotStyle)

zicross=floor(size(zs,1)/2);

figure

crossplot=drawHorCross(thetas,rs,valuemeshFromValues(values,zicross),fixedAxis,plotStyle);

updateTitle(valueTitle,zs,zicross)

slider = uicontrol('style','slider','min',1,'max',size(zs,1),'value',zicross,'position',[10 10 300 10]);
addlistener(slider,'ContinuousValueChange',@(hObject, event) updateplot(crossplot,floor(get(slider,'Value')),zs,values,valueTitle,plotStyle));
end

function updateplot(crossplot,zicross,zs,values,valueTitle,plotStyle)
    if strcmp(plotStyle,'pcolor')
        crossplot.CData=dataMakePeriodic(valuemeshFromValues(values,zicross));
    else
        crossplot.ZData=dataMakePeriodic(valuemeshFromValues(values,zicross));
    end
    updateTitle(valueTitle,zs,zicross)
end

function updateTitle(valueTitle,zs,zicross)
    title(['Cross section ' valueTitle ' for z=' num2str(zs(zicross),2)])
end

function valuemesh=valuemeshFromValues(values,zicross)
    valuemesh=values(:,:,zicross)';
end