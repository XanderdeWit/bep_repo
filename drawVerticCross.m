function h=drawVerticCross(rs,zs,data,dataLim,plotStyle)

[rmesh,zmesh]=meshgrid(rs,zs);

if strcmp(plotStyle,'pcolor')
    h=pcolor(rmesh,zmesh,data);
    shading interp
    colormap(getRudieColorMap())
    colorbar('TickLabelInterpreter','latex')
    caxis(dataLim)
elseif strcmp(plotStyle,'contourf')
    [~,h]=contourf(rmesh,zmesh,data);
    colormap(getRudieColorMap())
    colorbar('TickLabelInterpreter','latex')
    caxis(dataLim)
elseif strcmp(plotStyle,'surf')
    h=surf(rmesh,zmesh,data);
    zlim(dataLim)
else
    disp('Wrong plot style!')
    h=-1;
end

end

