function [meanTs,stdTs]=getNuStats(ts,print)

    meanTs=mean(ts,'Weighting','none');
    stdTs=std(ts,'Weighting','none');

    if print
        disp([ts.Name ' = ' num2str(meanTs) ' ' char(177) ' ' num2str(stdTs)])
    end
    
end