function [yfit,P,cP]=linearFit(x,y)
f = fit(x,y,'poly1');
P = coeffvalues(f);
cP = confint(f,0.95);
yfit = P(1)*x+P(2);
end