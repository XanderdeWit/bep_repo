classdef Domain
    
    properties
        ntheta,nthetam,thetas,thetamps,thetams,
        rs,rms,nr,nrm,rmsMesh,R
        zs,zms,nz,nzm,H
        horAreas
    end
    
    methods
        function qrm=qrmFromQr(d,qr)
            qrm=zeros(size(qr));

            %add wall zeros
            qr=[qr,zeros(size(qr,1),1)];

            for i=1:(size(qr,2)-1)
                qrm(:,i)=0.5*(qr(:,i)+qr(:,i+1));
            end
        end
        
        function vthm=vthmFromVth(d,vth)
            vthm=zeros(size(vth));

            %make periodic
            vthp=[vth;vth(1,:)];

            for i=1:(size(vthp,1)-1)
                vthm(i,:)=0.5*(vthp(i,:)+vthp(i+1,:));
            end
        end
    end
end

