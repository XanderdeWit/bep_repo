function cm=getRudieColorMap()

RdBu_basic=flipud([103,   0,  31;
                   178,  24,  43;
                   214,  96,  77;
                   244, 165, 130;
                   253, 219, 199;
                   247, 247, 247;
                   209, 229, 240;
                   146, 197, 222;
                    67, 147, 195;
                    33, 102, 172;
                     5,  48,  97])/255;
x_RdBu_basic=[0; 0.14; 0.23; 0.32; 0.41; 0.5; 0.59; 0.68; 0.77; 0.86; 1];
x_RdBu=(0:1:63)'/63;
RdBu=zeros(64,3);
for k=1:3
  RdBu(:,k)=interp1(x_RdBu_basic,RdBu_basic(:,k),x_RdBu);
end
cm=RdBu;

end