%Analayze the slabs: fit cosine to Vz profile in wall region and analyze inner Nu in order to find its uncertainty

%Choose which simulation to view
nsim=1;

%Totale time of the simulations in convective time units, to calculate the number of decorrelation times of Nu Inner
totalTs=[829.9, 710.6, 874.1, 925.8, 885.3];

slabname='slab8'; %slab at mid-height

simname=char(gSimnames(nsim));
rwallFinderOn=true; %rwallFinder does the localNu analyses based on the slabs (contrary to based on stafield, as in local_Nu.m)
ra=gRas(nsim);
pr=gPr;
dtPerSlab=0.5;
n1=gN1s(nsim);
n2=gN2s(nsim);

nrwall=gNrwalls(nsim);
rwall=d.rms(nrwall)

nrmode=findIn(d.rms,(max(d.rms)+rwall)/2) %analyze Vz at the center of the wall mode region
rmode=d.rms(nrmode)

offsets=zeros(1,n2-n1+1);

nuTots=zeros(1,n2-n1+1);
nuInners=zeros(1,n2-n1+1);
nuWalls=zeros(1,n2-n1+1);

%rwall finder
if rwallFinderOn
    nuLocal=zeros(d.nrm,n2-n1+1);
end

for n=n1:n2
    vz=h5read([gDataDir simname '/stst3/' slabname 'q3_' sprintf('%08d', n) '.h5'],'/var');
    temp=h5read([gDataDir simname '/stst3/' slabname 'dens_' sprintf('%08d', n) '.h5'],'/var');

    vzmode=vz(:,nrmode)';
    [~,xfit]=fitCos(vzmode,d.thetams);
    offsets(n-n1+1)=xfit(2);

    integrand=vz'.*temp'.*d.horAreas;

    nuTots(n-n1+1)=getNu(integrand,1:d.nrm,d.horAreas,ra,pr);
    nuInners(n-n1+1)=getNu(integrand,1:(nrwall-1),d.horAreas,ra,pr);
    nuWalls(n-n1+1)=getNu(integrand,nrwall:d.nrm,d.horAreas,ra,pr);
    
    %rwallFinder
    if rwallFinderOn
        for ri=1:d.nrm
            nuLocal(ri,n-n1+1)=getNu(integrand,ri:ri,d.horAreas,ra,pr);
        end
    end
end

%time average
meanNuTots=mean(nuTots);
meanNuInners=mean(nuInners);
meanNuWalls=mean(nuWalls);

%get time vector
t=linspace(0,dtPerSlab*(n2-n1),n2-n1+1);

%plot offsets
figure
scatter(t,mod(offsets,2*pi),10,'s','filled')
grid on
box on
title(['Offsets as a function of time for ' simname])

%Write results to csv
csvwrite(['offsets_results/' simname '_offsets.csv'],[t;mod(offsets,2*pi)])

%Plot the time series of the total, inner and wall Nu
figure
hold on
scatter(t,nuTots,10,'s','filled')
scatter(t,nuInners,10,'s','filled')
scatter(t,nuWalls,10,'s','filled')
line(xlim,[meanNuTots,meanNuTots])
line(xlim,[meanNuInners,meanNuInners])
line(xlim,[meanNuWalls,meanNuWalls])
hold off
grid on
box on
legend({'Nu total','Nu inner', 'Nu wall'},'Location','northeast')
title(['Nusselt numbers as a function of time for ' simname])

%Running average
figure
hold on
scatter(t,getRunningAverage(nuTots,false),10,'s','filled')
scatter(t,getRunningAverage(nuInners,false),10,'s','filled')
scatter(t,getRunningAverage(nuWalls,false),10,'s','filled')
line(xlim,[meanNuTots,meanNuTots])
line(xlim,[meanNuInners,meanNuInners])
line(xlim,[meanNuWalls,meanNuWalls])
hold off
grid on
box on
legend({'Nu total','Nu inner', 'Nu wall'},'Location','northeast')
title(['Running average Nusselt numbers as a function of time for ' simname])

%print results
getNuStats(timeseries(nuTots,t,'Name','Nu Total'),true);
[~,sigNuInner]=getNuStats(timeseries(nuInners,t,'Name','Nu Inner'),true);
getNuStats(timeseries(nuWalls,t,'Name','Nu Wall'),true);

%Do autocorrelation to find uncertainty
decorTime=autoCor(timeseries(nuInners,t),true);
title('Auto-correlation inner Nusselt number')
disp(['Decorrelation time inner Nu ' simname ' = ' num2str(decorTime)])
disp(['N inner Nu ' simname ' = ' num2str(totalTs(nsim)/decorTime)])
disp(['Delta Nu_Inner ' simname ' = ' num2str(sigNuInner/sqrt(totalTs(nsim)/decorTime))])

%rwall finder
if rwallFinderOn
    %time average
    meanNuLocal=mean(nuLocal,2);
    %Running spatial average (in r)
    weightsNuLocal=sum(d.horAreas,2);
    meanNuInner=getRunningAverage(meanNuLocal,weightsNuLocal);
    meanNuWall=flip(getRunningAverage(flip(meanNuLocal),flip(weightsNuLocal)));
    
    figure
    hold on
    scatter(d.rms,meanNuTots*ones(size(meanNuLocal)),10,'s','filled')
    scatter(d.rms,meanNuLocal,10,'s','filled')
    scatter(d.rms,meanNuInner,10,'s','filled')
    scatter(d.rms,meanNuWall,10,'s','filled')
    line([rwall,rwall],ylim)
    hold off
    grid on
    box on
    legend({'Nu total','Nu local','Nu inner', 'Nu wall'},'Location','northeast')
    title('Nusselt number as a function of r wall')
    xlabel('$r_{wall}$')
    ylabel('Nu')
end

function nu=getNu(integrand,interval,areas,ra,pr)
    integral=sum(sum(integrand(interval,:)));
    nu=integral*sqrt(ra*pr)/sum(sum(areas(interval,:)));
end