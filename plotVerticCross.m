function plotVerticCross(thetas,rs,zs,values,valueTitle,fixedAxis,plotStyle)

thetaicross=floor(size(thetas,2)/2);
thetaicross2=getThetaICross2(thetaicross,size(thetas,2));

figure
crossplot=drawVerticCross([flipud(-rs);rs],zs,valuemeshFromValues(values,thetaicross,thetaicross2),fixedAxis,plotStyle);

%[rmesh,zmesh]=meshgrid([flipud(-rs);rs],zs);

% figure
% [~,crossplot]=contourf(rmesh,zmesh,valuemeshFromValues(values,thetaicross,thetaicross2));
% colorbar

updateTitle(valueTitle,thetas,thetaicross,thetaicross2)

slider = uicontrol('style','slider','min',1,'max',size(thetas,2),'value',thetaicross,'position',[10 10 300 10]);
addlistener(slider,'ContinuousValueChange',@(hObject, event) updateplot(crossplot,floor(get(slider,'Value')),thetas,values,valueTitle,plotStyle));
end

function updateplot(crossplot,thetaicross,thetas,values,valueTitle,plotStyle)
    thetaicross2=getThetaICross2(thetaicross,size(thetas,2));
    
    if strcmp(plotStyle,'pcolor')
        crossplot.CData=valuemeshFromValues(values,thetaicross,thetaicross2);
    else
        crossplot.ZData=valuemeshFromValues(values,thetaicross,thetaicross2);
    end
    
    updateTitle(valueTitle,thetas,thetaicross,thetaicross2)
end

function updateTitle(valueTitle,thetas,thetaicross,thetaicross2)
    title(['Vertical cross section ' valueTitle ' for $\theta=' num2str(thetas(thetaicross)/pi,'%.2f') '\pi$ (and $\theta=' num2str(thetas(thetaicross2)/pi,'%.2f') '\pi$)'])
end

function valuemesh=valuemeshFromValues(values,thetaicross,thetaicross2)
    valuemesh=[fliplr(squeeze(values(thetaicross2,:,:))'), squeeze(values(thetaicross,:,:))'];
end

function thetaicross2=getThetaICross2(thetaicross,thetasize)
    thetaicross2=mod(thetaicross+ceil(thetasize/2)-1,thetasize)+1;
end