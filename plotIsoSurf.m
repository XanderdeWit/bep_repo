function plotIsoSurf(thetas,rs,zs,values,isoDefault,valueTitle)
[thetamesh,rmesh,zmesh]=meshgrid(thetas,rs,zs);
[xmesh,ymesh,zmesh]=pol2cart(thetamesh,rmesh,zmesh);
valuemesh=valuemeshFromValues(values);

figure

if size(isoDefault,2)==2
    p1=makeIsosurf(xmesh,ymesh,zmesh,valuemesh,isoDefault(1),'red');
    p2=makeIsosurf(xmesh,ymesh,zmesh,valuemesh,isoDefault(2),'blue');
    
    slider1 = uicontrol('style','slider','min',min(values(:)),'max',max(values(:)),'value',isoDefault(1),'position',[10 10 250 10]);
    slider2 = uicontrol('style','slider','min',min(values(:)),'max',max(values(:)),'value',isoDefault(2),'position',[300 10 250 10]);
    
    addlistener(slider1,'ContinuousValueChange',@(hObject, event) updateplot(p1,xmesh,ymesh,zmesh,valuemesh,get(slider1,'Value'),[get(slider1,'Value') get(slider2,'Value')],valueTitle));
    addlistener(slider2,'ContinuousValueChange',@(hObject, event) updateplot(p2,xmesh,ymesh,zmesh,valuemesh,get(slider2,'Value'),[get(slider1,'Value') get(slider2,'Value')],valueTitle));
    
    updateTitle(valueTitle,isoDefault)
else
    p=makeIsosurf(xmesh,ymesh,zmesh,valuemesh,isoDefault,'red');

    slider = uicontrol('style','slider','min',min(values(:)),'max',max(values(:)),'value',isoDefault,'position',[10 10 300 10]);
    addlistener(slider,'ContinuousValueChange',@(hObject, event) updateplot(p,xmesh,ymesh,zmesh,valuemesh,get(slider,'Value'),get(slider,'Value'),valueTitle));
    
    updateTitle(valueTitle,isoDefault)
end

daspect([1 1 1])
view(3); 
%axis tight
R=max(rs);
H=max(zs);
xlim([-R R])
ylim([-R R])
zlim([0 H])
camlight 
lighting gouraud

% maak een mooi cilindertje eromheen
% hold on
% xx=R*cos(2*pi*(0:1:500)/500);
% yy=R*sin(2*pi*(0:1:500)/500);
% zz=zeros(1,501);
% plot3(xx,yy,zz,'k-','linewidth',2)
% zz=ones(1,501)*max(zs);
% plot3(xx,yy,zz,'k-','linewidth',2)
% plot3([-R/2*sqrt(2) -R/2*sqrt(2)],[R/2*sqrt(2) R/2*sqrt(2)],[0 H],'k-','linewidth',2)
% plot3([R/2*sqrt(2) R/2*sqrt(2)],[-R/2*sqrt(2) -R/2*sqrt(2)],[0 H],'k-','linewidth',2)
% axis equal
% axis off
% hold off
end

function p=makeIsosurf(xmesh,ymesh,zmesh,valuemesh,iso,color)
    [faces,vertices]=isosurface(xmesh,ymesh,zmesh,valuemesh,iso);
    p=patch('Faces',faces,'Vertices',vertices);
    %isonormals(xmesh,ymesh,zmesh,valuemesh,p) --> doesn't work for non-uniform
    p.FaceColor = color;
    p.EdgeColor = 'none';
end

function updateplot(p,xmesh,ymesh,zmesh,valuemesh,iso,titleIso,valueTitle)
    [faces,vertices]=isosurface(xmesh,ymesh,zmesh,valuemesh,iso);
    p.Faces=faces;
    p.Vertices=vertices;
    updateTitle(valueTitle,titleIso)
end

function updateTitle(valueTitle,titleIso)
    if size(titleIso,2)==2
        title(['Iso surface ' valueTitle ' for ' num2str(titleIso(1)) ' (red) and ' num2str(titleIso(2)) ' (blue)'])
    else
        title(['Iso surface ' valueTitle ' for ' num2str(titleIso)])
    end
end

function valuemesh=valuemeshFromValues(values)
    valuemesh=permute(values,[2 1 3]);
end