%TODO implement R_wall and implement slider for z

%Analyzing case nsim1 till nsim2
nsim1=1;
nsim2=length(gSimnames);

for i=1:(nsim2-nsim1+1)
    nsim=nsim1+i-1;

    simname=char(gSimnames(nsim));

    vth_total=h5read([gDataDir simname '/stafield_data.h5'],'/q1_mean');
    averagingTime=h5read([gDataDir simname '/stafield_master.h5'],'/averaging_time');
    vth_ave=vth_total./double(averagingTime);

    %Line at certain z
    zi=round(d.nz/2); %mid-height

    figure
    scatter(d.rms,vth_ave(:,zi),10,'s','filled')
    title(['Vth averaged at z=' num2str(d.zms(zi),2) ' for ' simname])
    xlabel('r')
    grid on
    box on
    
end