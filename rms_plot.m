%Analyze RMS profiles

%Choose which quantity to analyze
quantity='dens';
quantityName='temperature';

%Put all simulations in single figure (true) or separate (false)
%For singleFig, the analysis at certain z is plotted in 1 fig
%For non-singleFig, more analyses are done (BL thickness) and vertical cross sections
singleFig=true;

%Analyzing case nsim1 till nsim2
nsim1=1;
nsim2=length(gSimnames);

if singleFig
    figure
end

for i=1:(nsim2-nsim1+1)
    nsim=nsim1+i-1;

    simname=char(gSimnames(nsim));

    %to add rwall
    nrwall=gNrwalls(nsim);
    rwall=d.rms(nrwall);

    %Choose appropriate coordinate definitions for this quantity
    rs=d.rms;
    %zs=d.zs(1:end-1);
    zs=d.zms;

    q2Total=h5read([gDataDir simname '/stafield_data.h5'],['/' quantity '_rms']);
    qTotal=h5read([gDataDir simname '/stafield_data.h5'],['/' quantity '_mean']);
    averagingTime=h5read([gDataDir simname '/stafield_master.h5'],'/averaging_time');

    qRMS=sqrt(q2Total./double(averagingTime)-(qTotal/double(averagingTime)).^2);

    %Line at certain z
    zi=round(d.nz/2); %mid-height
    %normalize
    %qRMS(:,zi)=qRMS(:,zi)/max(qRMS(:,zi));
    if ~singleFig
        figure
        title(['RMS of ' quantityName 'at z=' num2str(zs(zi),2) ' for ' simname])
    end
    hold on
    scatter(rs,qRMS(:,zi),10,'s','filled')
    xlabel('r')
    grid on
    box on
    
    %Add rwall
    if singleFig
        rmsWall(nsim)=qRMS(nrwall,zi); %add after plotting everything, in order not to screw up the legend
    else
        line([rwall,rwall],ylim) %add immediately
    end

    if ~singleFig
        %Vertical crossection
        figure
        drawVerticCross(rs,zs,qRMS','auto','pcolor');
        title(['RMS of ' quantityName ' (averaged in theta direction) for ' simname])
        %add rwall
        line([rwall,rwall],ylim,'Color','m')
        
        %find maximum RMS value to estimate BL thickness at z=zs(zi)
        [~, riMax]=max(qRMS(:,zi));
        disp(['Found maximum ' quantityName ' RMS at r=' num2str(rs(riMax)) ', ri=' num2str(riMax) ' (for z=' num2str(zs(zi)) ') '])
        disp(['Such that estimated BL thickness is ' num2str(max(rs) - rs(riMax))])
        
        %Line at certain r
        ri=round(d.nr*0.5); %near mid-radius
        figure
        scatter(qRMS(ri,:),zs,10,'s','filled')
        title(['RMS of ' quantityName 'at r=' num2str(rs(ri),2) ' for ' simname])
        ylabel('z')
        grid on
        box on
        
        %plot BL thickness of ekmanBL of r based on this quantity RMS peak
        blThicks=zeros(size(rs));
        for ri=1:length(rs)
            [~,zimax]=max(qRMS(ri,:));
            if zimax > d.nz/2
                blThicks(ri)=1-zs(zimax);
            else
                blThicks(ri)=zs(zimax);
            end
        end
        figure
        scatter(rs,blThicks,10,'s','filled')
        title(['Ekman BL thickness based on ' quantityName ' for ' simname])
        xlabel('r')
        %ylim([0 2e-3])
        grid on
        box on
        
        %plot BL thickness of sideBL of z based on this quantity RMS peak
        %start looking for max from ristart onwards
        ristart=230;
        rtot=0.1;
        blThicks=zeros(size(zs));
        for zi=1:length(zs)
            [~,rimax]=max(qRMS(ristart:end,zi));
            rimax=rimax+ristart-1;
            blThicks(zi)=rtot-rs(rimax);
        end
        
        figure
        scatter(blThicks,zs,10,'s','filled')
        title(['Side wall BL thickness based on ' quantityName ' for ' simname])
        ylabel('z')
        grid on
        box on
    end

end
if singleFig
    legend(gSimsLegend,'Location','northwest')
    scatter(d.rms(gNrwalls),rmsWall,100,'b','x') %add rwalls
    title(['RMS of ' quantityName 'at z=' num2str(zs(zi),2)])
end