%TODO implement R_wall


%Average flow field w.r.t. wall mode orientation

%Choose which simulation to view
nsim=5;

nrwall=gNrwalls(nsim);
rwall=d.rms(nrwall);

%Analyzing slab nslab1 till nslab2
nslab1=6;
nslab2=9;

simname=char(gSimnames(nsim));

n1=gN1s(nsim);
n2=gN2s(nsim);
offsets=csvread(['offsets_results/' simname '_offsets.csv']);
noffsets=size(offsets,2);
weight=1/noffsets;

%islab=8; %slab at mid-height
%slabname=['slab' num2str(islab)];

stst3locszi=[1,11,42,88,207,340,447,513,686];
stst3locsz=d.zms(stst3locszi);

useFixedAngularVelocity=false;
ang_vels=[0.014,0.023,0.031,0.058,0.064];
ang_vel=ang_vels(nsim);
dtPerSlab=0.5;

if (n2-n1+1)~=noffsets
    disp('Error: n2-n1+1 ~= noffsets')
else
    for i=1:(nslab2-nslab1+1)
        islab=nslab1+i-1;
        
        slabname=['slab' num2str(islab)];
        
        avz=0;
        avth=0;
        avrm=0;
        atemp=0;

        for j=1:noffsets
            n=n1-1+j;

            vz=h5read([gDataDir simname '/stst3/' slabname 'q3_' sprintf('%08d', n) '.h5'],'/var');
            vth=h5read([gDataDir simname '/stst3/' slabname 'q1_' sprintf('%08d', n) '.h5'],'/var');
            qr=h5read([gDataDir simname '/stst3/' slabname 'q2_' sprintf('%08d', n) '.h5'],'/var');
            temp=h5read([gDataDir simname '/stst3/' slabname 'dens_' sprintf('%08d', n) '.h5'],'/var');

            qrm=d.qrmFromQr(qr);
            vrm=qrm./d.rmsMesh;

            if useFixedAngularVelocity
                offset=mod(offsets(2,1)-(j-1)*dtPerSlab*ang_vel,2*pi);
            else
                offset=offsets(2,j);
            end
            ioffset=findIn(d.thetams,offset);

            avz=avz+weight.*[vz((ioffset+1):end,:); vz(1:ioffset,:)];
            avth=avth+weight.*[vth((ioffset+1):end,:); vth(1:ioffset,:)];
            avrm=avrm+weight.*[vrm((ioffset+1):end,:); vrm(1:ioffset,:)];
            atemp=atemp+weight.*[temp((ioffset+1):end,:); temp(1:ioffset,:)];
        end

        figure
        hold on
        drawHorCross(d.thetams,d.rms,avz','auto','pcolor');
        xlim([-0.1,0.1])
        ylim([-0.1,0.1])
        mcaxis=caxis;
        mampl=(mcaxis(2)-mcaxis(1))/2;
        caxis([-mampl,mampl]) %make color axis symmetric
        [x,y,u,v]=cartVectorfield(d.thetams,d.rms,vth,vrm,10);
        quiver(x,y,u,v,'Color','r')
        plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
        hold off
        title(['Average flow field at z=' num2str(stst3locsz(islab),2)])

        figure
        drawHorCross(d.thetams,d.rms,atemp','auto','pcolor');
        xlim([-0.1,0.1])
        ylim([-0.1,0.1])
        hold on
        plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
        hold off
        title(['Average temperature field at z=' num2str(stst3locsz(islab),2)])
        
        figure
        drawHorCross(d.thetams,d.rms,avth','auto','pcolor');
        xlim([-0.1,0.1])
        ylim([-0.1,0.1])
        mcaxis=caxis;
        mampl=(mcaxis(2)-mcaxis(1))/2;
        caxis([-mampl,mampl]) %make color axis symmetric
        hold on
        plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
        hold off
        title(['Average vth field at z=' num2str(stst3locsz(islab),2)])
        
        figure
        drawHorCross(d.thetams,d.rms,avrm','auto','pcolor');
        xlim([-0.1,0.1])
        ylim([-0.1,0.1])
        mcaxis=caxis;
        mampl=(mcaxis(2)-mcaxis(1))/2;
        caxis([-mampl,mampl]) %make color axis symmetric
        hold on
        plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
        hold off
        title(['Average vr field at z=' num2str(stst3locsz(islab),2)])
        
        figure
        drawHorCross(d.thetams,d.rms,avz','auto','pcolor');
        xlim([-0.1,0.1])
        ylim([-0.1,0.1])
        mcaxis=caxis;
        mampl=(mcaxis(2)-mcaxis(1))/2;
        caxis([-mampl,mampl]) %make color axis symmetric
        hold on
        plot(rwall*cos(0:pi/100:2*pi),rwall*sin(0:pi/100:2*pi),'m-','LineWidth',2)
        hold off
        title(['Average vz field at z=' num2str(stst3locsz(islab),2)])
    end
end


function [x,y,u,v]=cartVectorfield(thetas,rs,vth,vrm,skip)
    x=zeros(size(vth));
    y=zeros(size(vth));
    u=zeros(size(vth));
    v=zeros(size(vth));
    
    for i=1:size(x,1)
        for j=1:size(x,2)
            theta=thetas(i);
            r=rs(j);
            
            x(i,j)=r*cos(theta);
            y(i,j)=r*sin(theta);
            
            u(i,j)=-vth(i,j)*sin(theta)+vrm(i,j)*cos(theta);
            v(i,j)=vth(i,j)*cos(theta)+vrm(i,j)*sin(theta);
        end
    end
    
    x=x(1:skip:end,1:skip:end);
    y=y(1:skip:end,1:skip:end);
    u=u(1:skip:end,1:skip:end);
    v=v(1:skip:end,1:skip:end);
    
    %x=blockMean(x,skip);
    %y=blockMean(y,skip);
    %u=blockMean(u,skip);
    %v=blockMean(v,skip);
    
end

function Y = blockMean(X, V, W)
% 2D block mean over 1st and 2nd dim [MEX]
% The mean of V*W elements along the 1st and 2nd dimensions is calculated.
% Y = BlockMean(X, V, W)
% INPUT:
%   X: UINT8 or DOUBLE array of any size.
%   V, W: Scalar numerical with integer value. Each element of the output is
%      the mean over V*W neighbouring elements of the input.
%      V and W are limited to 256 to limit memory usage.
%      A square V*V block is used, if W is omitted.
% OUTPUT:
%   Y: UINT8 or DOUBLE array, the 1st and 2nd dimensions are V and W times
%      shorter: [FLOOR(X / V) x FLOOR(Y / W) x (further dims...)]
%      If the size of the 1st or 2nd dimension is not a multiple of V and W,
%      the remaining elements at the end are skipped.
%      The empty matrix is replied for empty inputs or if the 1st or 2nd
%      dimension is shorter than V or W.
%
% NOTE: This is implemented for DOUBLE and UINT8, because I used it for
%   anti-aliasing of images stored as 3D RGB arrays.
%
% COMPILE: See BlockMean.c
%
% TESTING: Run uTest_BlockMean to check validity and speed.
%
% Tested: Matlab 6.5, 7.7, 7.8, WinXP
% Author: Jan Simon, Heidelberg, (C) 2009-2010 matlab.THISYEAR(a)nMINUSsimon.de
% $JRev: R0r V:013 Sum:DzKusULxGsEb Date:22-Sep-2010 02:30:25 $
% $License: BSD (see Docs\BSD_License.txt) $
% $UnitTest: uTest_BlockMean $
% $File: Tools\GLMath\BlockMean.m $
% History:
% 001: 20-Jul-2009 23:22, Generalized ChunkMeanRGB: Free trailing dimensions.
% 010: 12-Mar-2010 09:34, Rectangular blocks. TestBlockMean fixed.
%      The TestBlockMean published on the FEX at 21-Jul-2009 missed the function
%      isEqualTol - sorry.
% ==============================================================================
% This M-function is just a proof of concept and used for testing the results
% of the MEX version. The MEX version has a better check of the inputs.
% If M- and MEX-version are found in the path, the MEX is preferred.
% Get size of X and calculate the reduced sizes for the 1st and 2nd dimension:
if nargin < 3
   W = V;
end
S = size(X);
M = S(1) - mod(S(1), V);
N = S(2) - mod(S(2), W);
if M * N == 0
   Y = X([]);  % Copy type of X
   return;
end
MV = M / V;
NW = N / W;
% Cut and reshape input such that the 1st and 3rd dimension have the lengths V
% and W:
XM = reshape(X(1:M, 1:N, :), V, MV, W, NW, []);
% Different methods depending on the type of the input:
if isa(X, 'double')
   Y = sum(sum(XM, 1), 3) .* (1.0 / (V * W));
elseif uint8(0.8) == 1  % UINT8 of Matlab7 rounds:
   % NOT .*1/(V*W) !!! Otherwise the rounding differs from C-Mex
   Y = uint8(sum(sum(XM, 1), 3) ./ (V * W));
else                    % UINT8 of Matlab6 truncates, so round manually:
   Y = uint8(round(sum(sum(XM, 1), 3) ./ (V * W)));
end
% Remove singleton dimensions:
S(1) = MV;
S(2) = NW;
Y    = reshape(Y, S);
return;

end