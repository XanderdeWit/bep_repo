%initialize grid

%Grid is the same for all simulations, so nsim=1 can be used
simname=char(gSimnames(1));

d=Domain;

%Uniform grid in azimuthal direction
d.ntheta=769; %NOTE: number of theta grid points is hardcoded here
d.nthetam=d.ntheta-1;
d.thetas=linspace(0,2*pi,d.ntheta);
thetamps=linspace(0,2*pi,d.ntheta)+0.5*2*pi/d.nthetam;
d.thetams=thetamps(1:(length(thetamps)-1));

%Radial grid
radcor = fopen([gDataDir simname '/radcor.out'],'r');
radcordata = fscanf(radcor,'%f %f\n',[5 Inf]);
d.rs = radcordata(2,:)';
rms = radcordata(3,:)';
d.rms = rms(1:(length(rms)-1));
d.nr = length(d.rs);
d.nrm = length(d.rms);
d.rmsMesh=repmat(d.rms',length(d.thetams),1);
d.R=max(d.rs);

%Axial grid
axicor = fopen([gDataDir simname '/axicor.out'],'r');
axicordata = fscanf(axicor,'%f %f\n',[5 Inf]);
d.zs = axicordata(2,:)';
zms = axicordata(3,:)';
d.zms = zms(1:(length(zms)-1));
d.nz = length(d.zs);
d.nzm = length(d.zms);
d.H=max(d.zs);

%Areas in horizontal cross section
horAreas=zeros(d.nrm,d.nthetam);
for ir=1:d.nrm
    for itheta=1:d.nthetam
        horAreas(ir,itheta)=pi*((d.rs(ir+1))^2-(d.rs(ir))^2)/d.nthetam;
    end
end
d.horAreas=horAreas;

%Plot mesh
zlin=linspace(0,1);
rlin=linspace(0,max(d.rs));

rplot=d.rs+0*zlin;
zplot=d.zs+0*rlin;

figure
plot(rplot,zlin,'b')
hold on
plot(rlin,zplot,'b')
hold off
title('Domain discretization')
ylabel('z')
xlabel('r')
grid off