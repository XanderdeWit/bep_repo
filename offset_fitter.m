%Analyze the wall mode rotation, uses the .csv files in the 'offsets_results' directory coming from the cosine fits as produced in slabs.m

%Put all simulations in single figure (true) or separate (false)
singleFig=false;

%Analyzing case nsim1 till nsim2
nsim1=1;
nsim2=length(gSimnames);

%Number of parts to divide time series in to calculate uncertainty
partss=[6,6,3,10,10];

stds=zeros(1,nsim2-nsim1+1);
ang_vel=zeros(1,nsim2-nsim1+1);
d_ang_vel=zeros(1,nsim2-nsim1+1);
period=zeros(1,nsim2-nsim1+1);
ratio=zeros(1,nsim2-nsim1+1);

if singleFig
    figure
end

for i=1:(nsim2-nsim1+1)
    nsim=nsim1+i-1;
    
    simname=char(gSimnames(nsim));

    offsets=csvread(['offsets_results/' simname '_offsets.csv']);

    %fix sim13e10 (the fitting failed after 193 time units)
    if nsim==3
        offsets=offsets(:,1:193*2);
        if singleFig
            xlim([0,max(offsets(1,:))])
        end
    end
    
    %Put all angles on one line
    noffsets=size(offsets,2);
    patience=40*2; %don't switch again if you recently switched
    waited=patience;
    for j=1:(noffsets-1)
        waited=waited+1;
        if waited > patience
            if abs(offsets(2,noffsets-j)-offsets(2,noffsets-j+1))>3.2 %check for jumps in the angle (from right to left)
                waited=0;
                for k=1:(noffsets-j)
                    offsets(2,k)=offsets(2,k)+2*pi;
                end
            end
        end
    end
    offsets(2,:)=offsets(2,:)-min(offsets(2,:));

    %fix: push back outliers
    yfit=linearFit(offsets(1,:)',offsets(2,:)');
    for i=1:noffsets
        if offsets(2,i)-yfit(i) > 3.2
            offsets(2,i)=offsets(2,i)-2*pi;
        end
    end
    
    %make it a positive rotation
    offsets(2,:)=-offsets(2,:)+max(offsets(2,:));

    %plot offsets
    if ~singleFig
        figure
        title(['Rotation of the wall mode as a function of time for ' simname])
    end
    hold on
    scatter(offsets(1,:),-offsets(2,:),10,'s','filled')
    grid on
    box on
    xlabel('Convective time')
    ylabel('Angular rotation (in rad)')

    %linear fit
    [yfit,P,cP]=linearFit(offsets(1,:)',offsets(2,:)');
    
    %Calculate uncertainty
    parts=partss(nsim);
    noffsets=size(offsets,2);
    prcs=zeros(1,parts);
    for j=1:parts
        intvl=round(noffsets/parts*(j-1)+1):round(noffsets/parts*j);
        [~,pP,~]=linearFit(offsets(1,intvl)',offsets(2,intvl)');
        prcs(j)=pP(1);
    end
    stds(nsim)=std(prcs)/sqrt(parts);
    
    %Plot fit
    rc=P(1);
    if ~singleFig
        plot(offsets(1,:),-yfit,'r-.');
        mylim=ylim;
        ylim([mylim(1),0])
    end
    
    %Save results into arrays
    ang_vel(nsim)=rc; %in rad/convective time unit
    d_ang_vel(nsim)=rc-cP(1,1);
    period(nsim)=2*pi/ang_vel(nsim); %in convective time unit / revelution
    ratio(nsim)=(1/(2*gRos(nsim)))/ang_vel(nsim); %ratio between angular velocity of cylinder and of the wallmode
end

if singleFig
    legend(gSimsLegend,'Location','southwest')
    title('Rotation of the wall mode as a function of time')
end

%Make graph of angular velocity vs Ra and fit power law
figure
x=gRas;
y=ang_vel;
scatter(x,y,60,'d','filled')

grid on
box on
title('Angular velocity vs Ra')

set(gca,'xscale','log')
xlim([0.9*min(x),1.1*max(x)])
set(gca,'yscale','log')
ylim([0.9*min(y),1.1*max(y)])

%Fit power law
[~,P,cP]=linearFit(log(x)',log(y)');
power=P(1)
dPower=[P(1)-cP(1,1),cP(2,1)-P(1)] %uncertainty in power
base=exp(P(2))
dBase=[base-exp(cP(1,2)),exp(cP(2,2))-base] %uncertainty in prefactor
hold on
plot(x,base*x.^power,'r-.');
errorbar(x, y, stds, 'LineStyle','none');
hold off