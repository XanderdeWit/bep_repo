%Analyze the Nusselt numbers as time series

%Choose which simulation to view
nsim=1;

simname=char(gSimnames(nsim));

file1 = fopen([gDataDir simname '/nusse.out'],'r');
data1 = fscanf(file1,'%f %f\n',[2 Inf]);
tsNuVol=timeseries(data1(2,:)',data1(1,:)','Name','Volume average');

file2 = fopen([gDataDir simname '/nusse2.out'],'r');
data2 = fscanf(file2,'%f %f\n',[3 Inf]);
tsNuW1=timeseries(data2(2,:)',data2(1,:)','Name','Wall low');
tsNuW2=timeseries(data2(3,:)',data2(1,:)','Name','Wall high');

file3 = fopen([gDataDir simname '/nusse3.out'],'r');
data3 = fscanf(file3,'%f %f\n',[3 Inf]);
tsNuTe=timeseries(data3(2,:)',data3(1,:)','Name','Viscous dissipation');
tsNuTh=timeseries(data3(3,:)',data3(1,:)','Name','Thermal dissipation');

%A mistake in the code needs to be fixed in post-pro
tsNuTe=fixNuTe(tsNuTe,gPr);

tss=[tsNuVol,tsNuW1,tsNuW2,tsNuTe,tsNuTh];

%Interpolate to uniform time grid and remove duplicate points
tss=interpTss(tss);

%Plot time series
figure
hold on
for i=1:length(tss)
   scatter(tss(i).Time,tss(i).Data,10,'s','filled')
end
hold off
title(['Nusselt numbers ' simname])
ylabel('Nu')
xlabel('Convective time')
legend({tss.Name},'Location','northeast')
%ylim([0 150])
grid on
box on
%ylim([0,2700])
%hold on
%breakyaxis([200 2100]);
%hold off


%Show running average and print stats
figure
hold on
for i=1:length(tss)
   %Print stats
   getNuStats(tss(i),true);
   
   scatter(tss(i).Time,getRunningAverage(tss(i).Data,false),10,'s','filled')
end
hold off
title(['Nusselt numbers ' simname ' running average'])
ylabel('Average Nu')
xlabel('Convective time')
legend({tss.Name},'Location','northeast')
%ylim([0 150])
grid on
box on

%Autocorrelation
for i=1:5

nNu=i;

decorTime=autoCor(tss(nNu),true);

disp(['Decorrelation time ' num2str(nNu) ' = ' num2str(decorTime)])
disp(['N ' num2str(nNu) ' = ' num2str(max(tss(nNu).Time)/decorTime)]) %number of decorTimes in the simulation

end

function tss=interpTss(tss)

    for i=1:length(tss)
        nt=length(tss(i).Time);
        tnew=linspace(0,tss(i).Time(nt),nt);
        [tUnique,iUnique]=unique(tss(i).Time);
        tss(i)=timeseries(interp1(tUnique,tss(i).Data(iUnique),tnew,'linear','extrap'),tnew,'Name',tss(i).Name);
    end

end

function fixedTs=fixNuTe(ts,pr)

    ts.Data=pr.*(ts.Data-1)+1;
    fixedTs=ts;

end